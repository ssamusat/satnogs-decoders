---
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - template: 'Security/Dependency-Scanning.gitlab-ci.yml'
  - template: 'Security/SAST.gitlab-ci.yml'
  - template: 'Security/Secret-Detection.gitlab-ci.yml'
variables:
  GITLAB_CI_IMAGE_ALPINE: 'alpine:3.18.0'
  GITLAB_CI_IMAGE_PYTHON: 'python:3.9.12'
  GITLAB_CI_IMAGE_KAITAI: 'librespace/kaitai:0.9'
  GITLAB_CI_SIGN_OFF_EXCLUDE: '5a731984a23981472ef1ec7fddfd2315fe82c9f5'
stages:
  - compile
  - static
  - build
  - test
  - deploy
  - trigger
  - security

# 'compile' stage
compile:
  stage: compile
  needs: []
  image:
    name: ${GITLAB_CI_IMAGE_KAITAI}
    entrypoint: [""]
  script:
    - /usr/bin/ksc --target python --outdir satnogsdecoders/decoder ksy/*.ksy
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - satnogsdecoders/decoder

# 'static' stage
sign_off:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache git
  script: >-
    git log
    --grep "^Signed-off-by: .\+<.\+\(@\| at \).\+\(\.\| dot \).\+>$"
    --invert-grep
    --format="Detected commit '%h' with missing or bad sign-off! Please read 'CONTRIBUTING.md'."
    --exit-code
    $(rev=$(git rev-parse -q --verify "$GITLAB_CI_SIGN_OFF_EXCLUDE^{commit}") && echo "$rev..")

static:
  stage: static
  needs:
    - job: compile
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - tox run-parallel -e "yamllint,flake8,isort,yapf,pylint"

# 'build' stage
build:
  stage: build
  needs:
    - job: compile
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - rm -rf dist
    - tox run -e "build"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - dist

# 'test' stage
deps:
  stage: test
  needs:
    - job: compile
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - tox run -e "deps"

test:
  stage: test
  needs:
    - job: compile
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - tox run -e "pytest"

# 'deploy' stage
deploy:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - rm -rf dist
    - tox run -e "upload"
  only:
    refs:
      - tags
    variables:
      - $PYPI_USERNAME
      - $PYPI_PASSWORD

# 'trigger' stage
trigger:
  stage: trigger
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache curl git
  script:
    # XXX: Workaround for GitLab missing latest tag pipeline triggering feature
    - |
      SATNOGS_DB_LATEST="${SATNOGS_DB_GIT_URL:+$(git ls-remote -q --tags --refs --exit-code --sort="v:refname" "$SATNOGS_DB_GIT_URL" | awk 'BEGIN { FS="/" } END { print $3 }')}"
      PIPELINE_TRIGGERS=$(echo "$PIPELINE_TRIGGERS" | sed 's/{{SATNOGS_DB_LATEST}}/'"$SATNOGS_DB_LATEST"'/g')
      [ -z "$CI_COMMIT_TAG" ] || PIPELINE_TRIGGERS=$(echo "$PIPELINE_TRIGGERS" | sed -e 's/{{SATNOGS_DECODERS_VERSION}}/'"$CI_COMMIT_TAG"'/g')
    - for trigger in $PIPELINE_TRIGGERS; do curl -X POST "$trigger"; done
  only:
    refs:
      - tags
    variables:
      - $PIPELINE_TRIGGERS

# 'security' stage
dependency_scanning:
  stage: security
  needs:
    - job: compile
      artifacts: true
  variables:
    DS_DEFAULT_ANALYZERS: 'gemnasium,gemnasium-python'
sast:
  stage: security
  needs:
    - job: compile
      artifacts: true
secret_detection:
  stage: security
  needs:
    - job: compile
      artifacts: true
